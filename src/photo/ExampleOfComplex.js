import React, { Component } from 'react';
import './Photo.css';
import { Avatar, Icon } from 'antd';
import { Link } from 'react-router-dom';
import { getAvatarColor } from '../util/Colors';
import { formatDateTime } from '../util/Helpers';

import { Radio, Button } from 'antd';
const RadioGroup = Radio.Group;

class Photo extends Component {
    calculatePercentage = (choice) => {
        if(this.props.photo.totalVotes === 0) {
            return 0;
        }
        return (choice.voteCount*100)/(this.props.photo.totalVotes);
    };

    isSelected = (choice) => {
        return this.props.photo.selectedChoice === choice.id;
    }

    getWinningChoice = () => {
        return this.props.photo.choices.reduce((prevChoice, currentChoice) =>
            currentChoice.voteCount > prevChoice.voteCount ? currentChoice : prevChoice, 
            {voteCount: -Infinity}
        );
    }

    getTimeRemaining = (photo) => {
        const expirationTime = new Date(photo.expirationDateTime).getTime();
        const currentTime = new Date().getTime();
    
        var difference_ms = expirationTime - currentTime;
        var seconds = Math.floor( (difference_ms/1000) % 60 );
        var minutes = Math.floor( (difference_ms/1000/60) % 60 );
        var hours = Math.floor( (difference_ms/(1000*60*60)) % 24 );
        var days = Math.floor( difference_ms/(1000*60*60*24) );
    
        let timeRemaining;
    
        if(days > 0) {
            timeRemaining = days + " days left";
        } else if (hours > 0) {
            timeRemaining = hours + " hours left";
        } else if (minutes > 0) {
            timeRemaining = minutes + " minutes left";
        } else if(seconds > 0) {
            timeRemaining = seconds + " seconds left";
        } else {
            timeRemaining = "less than a second left";
        }
        
        return timeRemaining;
    }

    render() {
        const photoChoices = [];
        if(this.props.photo.selectedChoice || this.props.photo.expired) {
            const winningChoice = this.props.photo.expired ? this.getWinningChoice() : null;

            this.props.photo.choices.forEach(choice => {
                photoChoices.push(<CompletedOrVotedPhotoChoice
                    key={choice.id} 
                    choice={choice}
                    isWinner={winningChoice && choice.id === winningChoice.id}
                    isSelected={this.isSelected(choice)}
                    percentVote={this.calculatePercentage(choice)} 
                />);
            });                
        } else {
            this.props.photo.choices.forEach(choice => {
                photoChoices.push(<Radio className="photo-choice-radio" key={choice.id} value={choice.id}>{choice.text}</Radio>)
            })    
        }        
        return (
            <div className="photo-content">
                <div className="photo-header">
                    <div className="photo-creator-info">
                        <Link className="creator-link" to={`/users/${this.props.photo.createdBy.username}`}>
                            <Avatar className="photo-creator-avatar"
                                style={{ backgroundColor: getAvatarColor(this.props.photo.createdBy.name)}} >
                                {this.props.photo.createdBy.name[0].toUpperCase()}
                            </Avatar>
                            <span className="photo-creator-name">
                                {this.props.photo.createdBy.name}
                            </span>
                            <span className="photo-creator-username">
                                @{this.props.photo.createdBy.username}
                            </span>
                            <span className="photo-creation-date">
                                {formatDateTime(this.props.photo.creationDateTime)}
                            </span>
                        </Link>
                    </div>
                    <div className="photo-question">
                        {this.props.photo.question}
                    </div>
                </div>
                <div className="photo-choices">
                    <RadioGroup 
                        className="photo-choice-radio-group"
                        onChange={this.props.handleVoteChange} 
                        value={this.props.currentVote}>
                        { photoChoices }
                    </RadioGroup>
                </div>
                <div className="photo-footer">
                    { 
                        !(this.props.photo.selectedChoice || this.props.photo.expired) ?
                        (<Button className="vote-button" disabled={!this.props.currentVote} onClick={this.props.handleVoteSubmit}>Vote</Button>) : null 
                    }
                    <span className="total-votes">{this.props.photo.totalVotes} votes</span>
                    <span className="separator">•</span>
                    <span className="time-left">
                        {
                            this.props.photo.expired ? "Final results" :
                            this.getTimeRemaining(this.props.photo)
                        }
                    </span>
                </div>
            </div>
        );
    }
}

function CompletedOrVotedPhotoChoice(props) {
    return (
        <div className="cv-photo-choice">
            <span className="cv-photo-choice-details">
                <span className="cv-choice-percentage">
                    {Math.round(props.percentVote * 100) / 100}%
                </span>            
                <span className="cv-choice-text">
                    {props.choice.text}
                </span>
                {
                    props.isSelected ? (
                    <Icon
                        className="selected-choice-icon"
                        type="check-circle-o"
                    /> ): null
                }    
            </span>
            <span className={props.isWinner ? 'cv-choice-percent-chart winner': 'cv-choice-percent-chart'} 
                style={{width: props.percentVote + '%' }}>
            </span>
        </div>
    );
}


export default Example
;